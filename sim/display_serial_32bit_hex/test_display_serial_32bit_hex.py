import cocotb
from cocotb.triggers import FallingEdge, RisingEdge
from cocotb.clock import Clock


@cocotb.test()
async def test_load_data(dut):
    """Test that data is loaded on a clock edge only when data_load_i, and is
    otherwise ignored"""
    clock = Clock(dut.clk_i, 1, units="ns")
    cocotb.start_soon(clock.start())
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    dut.data_i.value = 0xdeadbeef
    dut.data_load_i.value = 1
    await FallingEdge(dut.clk_i)
    assert dut.data.value == 0xdeadbeef

    # value isn't loaded while not data_load_i
    dut.data_i.value = 0xc0fecafe
    dut.data_load_i.value = 0
    await FallingEdge(dut.clk_i)
    assert dut.data.value == 0xdeadbeef

    # new value loaded while data_load_i
    dut.data_load_i.value = 1
    await FallingEdge(dut.clk_i)
    assert dut.data.value == 0xc0fecafe

@cocotb.test()
async def test_display_data(dut):
    """Test that loaded data is clocked out onto the serial bus correctly"""
    expected_frames = [
        # note segments are inverted sense
        # 32107654   .gfedcba
        ('10100001' '00010000'), # d:0
        ('10000110' '00100000'), # e:1
        ('10001000' '01000000'), # a:2
        ('10100001' '10000000'), # d:3
        ('10000011' '00000001'), # b:4
        ('10000110' '00000010'), # e:5
        ('10000110' '00000100'), # e:6
        ('10001110' '00001000'), # f:7
    ]

    clock = Clock(dut.clk_i, 1, units="ns")
    cocotb.start_soon(clock.start())
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    dut.data_i.value = 0xdeadbeef
    dut.data_load_i.value = 1
    await FallingEdge(dut.clk_i)

    for frame in expected_frames:
        for frame_bit in frame:
            await RisingEdge(dut.clk_i)
            # expect sdo_o to match the expected frame
            assert dut.sdo_o.value.binstr == frame_bit
            # expect rck_o high during ready for low-high edge after frame
            assert dut.rck_o.value == 1
        await RisingEdge(dut.clk_i)
        assert dut.rck_o.value == 0
