import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, ClockCycles
from cocotb.clock import Clock


@cocotb.test()
async def test_reset(dut):
    """Test that output signal is reset to corerct idle value"""
    clock = Clock(dut.clk_i, 562.5, units="us")
    cocotb.start_soon(clock.start())
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    assert dut.baseband_o.value == 0


@cocotb.test()
async def test_leader(dut):
    """Test that output signal emits leader correctly"""
    clock = Clock(dut.clk_i, 562.5, units="us")
    cocotb.start_soon(clock.start())
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    # after first clock, leader begins and continues for 16 clocks total
    for i in range(16):
        await FallingEdge(dut.clk_i)
        assert dut.baseband_o.value == 1

    # after 16 clocks of high leader, expect 8 of low
    for i in range(8):
        await FallingEdge(dut.clk_i)
        assert dut.baseband_o.value == 0

@cocotb.test()
async def test_data(dut):
    """Test that output signal emits data correctly"""
    clock = Clock(dut.clk_i, 562.5, units="us")
    cocotb.start_soon(clock.start())
    dut.data_i.value = 0xde21be41
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    async def assert_nec_symbol(dut, spaces):
        await FallingEdge(dut.clk_i)
        assert dut.baseband_o.value == 1
        for i in range(spaces):
            await FallingEdge(dut.clk_i)
            assert dut.baseband_o.value == 0

    async def assert_nec_0(dut):
        await assert_nec_symbol(dut, 1)

    async def assert_nec_1(dut):
        await assert_nec_symbol(dut, 3)

    await ClockCycles(dut.clk_i, 24, rising=False)
    # note bit order swapped, but byte order is not
    expected_message = ''.join(map(lambda x: format(x, '08b'), [0x7b, 0x84, 0x7d, 0x82]))
    for bit in expected_message:
        assert bit in ('1', '0')
        if bit == '1':
            await assert_nec_1(dut)
        else:
            await assert_nec_0(dut)

@cocotb.test()
async def test_eom(dut):
    """Test that output signal emits EOM pulse after data"""
    clock = Clock(dut.clk_i, 562.5, units="us")
    cocotb.start_soon(clock.start())
    dut.rst_i.value = 1
    await FallingEdge(dut.clk_i)
    dut.rst_i.value = 0

    # 32*3 - each data bit is complemented. '1' is 2 clocks, '0' is 1 clocks
    await ClockCycles(dut.clk_i, 24+32*3+1, rising=False)
    assert dut.baseband_o.value == 1

    # check we go off, and don't e.g. repeat the message
    for i in range(8):
        await FallingEdge(dut.clk_i)
        assert dut.baseband_o.value == 0
