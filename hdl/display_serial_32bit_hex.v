module display_serial_32bit_hex(
	input clk_i,
	input rst_i,

	// data input register, 32 bits wide, given 4 bits per digit of 8-digit
	// 7-segment display
	input [31:0] data_i,
	input data_load_i,

	// serial stream out
	output reg rck_o,
	output sck_o,
	output sdo_o
);

// FIXME look at parameterising the width. Challenging because of digit_lookup?
reg [15:0]shift;  // serial frame for current digit. 8-bit segment selection plus 8-bit digit selection
reg [31:0]data;   // user data to display in hex on display
reg [2:0]digit;   // digit position being clocked out (0: left, 7: right)
reg [3:0]bit_num; // bit position in serial frame currently on the wire
reg state;        // Current state. One of s_xfer or s_latch.
parameter s_xfer=0, s_latch=1;


assign sdo_o = shift[15];
assign sck_o = clk_i & (state == s_xfer);

function [7:0]segment_lookup;
	input [3:0]value;
begin
	case (value)
		                      // .gfedcba
		'h0: segment_lookup = 8'b00111111;
		'h1: segment_lookup = 8'b00000110;
		'h2: segment_lookup = 8'b01011011;
		'h3: segment_lookup = 8'b01001111;
		'h4: segment_lookup = 8'b01100110;
		'h5: segment_lookup = 8'b01101101;
		'h6: segment_lookup = 8'b01111101;
		'h7: segment_lookup = 8'b00000111;
		'h8: segment_lookup = 8'b01111111;
		'h9: segment_lookup = 8'b01101111;
		'ha: segment_lookup = 8'b01110111;
		'hb: segment_lookup = 8'b01111100;
		'hc: segment_lookup = 8'b00111001;
		'hd: segment_lookup = 8'b01011110;
		'he: segment_lookup = 8'b01111001;
		'hf: segment_lookup = 8'b01110001;
	endcase
end
endfunction

function [7:0]digit_lookup;
	input [2:0] digit;
begin
	case (digit)
		0: digit_lookup = 8'h10;
		1: digit_lookup = 8'h20;
		2: digit_lookup = 8'h40;
		3: digit_lookup = 8'h80;
		4: digit_lookup = 8'h01;
		5: digit_lookup = 8'h02;
		6: digit_lookup = 8'h04;
		7: digit_lookup = 8'h08;
	endcase
end
endfunction

function [3:0]digit_data;
	input [2:0]digit;
	input [31:0]data;
begin
	// bit     | ... | 11 10 9 8 | 7 6 5 4 | 3 2 1 0
	// digit   | ... | digit 5   | digit 6 | digit 7
	digit_data = data[(7-digit)*4+3 -: 4];
end
endfunction

always @(posedge clk_i) begin
	// to do: loading data here could change display between digits being
	// clocked out, displaying a single frame of incorrect data. Probably not
	// an issue as a once-off but might cause display problems with rapidly
	// changing data.
	// Ideally we should hold off loading the actual data register until the
	// end of the digit strobe/sweep
	if (data_load_i)
		data <= data_i;
end

always @(negedge clk_i) begin
	if (rst_i == 1'b1) begin
		bit_num <= 0;
		digit <= 0;
		state <= s_latch;
	end else begin
		case (state)
			// Transfer the serial frame onto the wire and into the display
			s_xfer: begin
				case (bit_num)
					15: begin
						rck_o <= 0;
						digit <= digit + 1;
						state <= s_latch;
					end
					default: begin
						shift <= { shift [14:0], 1'b1 };
					end
				endcase
				bit_num <= bit_num + 1;
			end
			// Latch the remote, complete serial frame into the display's outputs
			s_latch: begin
				rck_o <= 1;
				bit_num <= 0;
				shift[7:0] <= digit_lookup(digit);
				// invert segments, active low
				shift[15:8] <= ~segment_lookup(digit_data(digit, data));
				state <= s_xfer;
			end
		endcase
	end
end

`ifdef COCOTB_SIM
initial begin
	$dumpfile("display_serial_32bit_hex.vcd");
	$dumpvars(0, display_serial_32bit_hex);
end
`endif
endmodule
