module nec_remote(
	input clk_i,
	input rst_i,
	input [31:0] data_i,

	output reg baseband_o
);

parameter s_done = 0, s_leader = 1, s_mark = 2, s_space3 = 3, s_space2 = 4, s_space1 = 5;

reg [2:0] state;
reg [4:0] leader_clock;
reg [31:0] data;
reg [6:0] data_bit;

always @(posedge clk_i) begin
	if (rst_i == 1'b1) begin
		baseband_o <= 1'b0;
		leader_clock <= 0;
		// swap bytes LSB first
		data[31:0] <= {data_i[7:0], data_i[15:8], data_i[23:16], data_i[31:24]};
		data_bit <= 0;
		state <= s_leader;
	end else begin
		case (state)
			s_leader: begin
				if (leader_clock < 16) begin
					baseband_o <= 1;
				end else if (leader_clock < 23) begin
					// 23 not 24, as we have one clock overhang after state change
					baseband_o <= 0;
				end else begin
					// begin symbol - both symbols start with mark, space varies
					state <= s_mark;
				end
				leader_clock <= leader_clock + 1;
			end
			s_mark: begin
				baseband_o <= 1'b1;
				data[30:0] <= data[31:1];
				data_bit <= data_bit + 1;
				if (data_bit == 32) begin
					state <= s_done;
				end else if (data[0] == 1'b1) begin
					// enter long space for 1
					state <= s_space3;
				end else begin
					// enter short space for 0
					state <= s_space1;
				end
			end
			s_space3: begin
				baseband_o <= 1'b0;
				state <= s_space2;
			end
			s_space2: begin
				baseband_o <= 1'b0;
				state <= s_space1;
			end
			s_space1: begin
				baseband_o <= 1'b0;
				state <= s_mark;
			end
			s_done: begin
				baseband_o <= 1'b0;
			end
		endcase
	end
end
`ifdef COCOTB_SIM
initial begin
	$dumpfile("nec_remote.vcd");
	$dumpvars(0, nec_remote);
end
`endif
endmodule
