# Miscellaneous IP cores

A place for me to put common IP cores I use for debugging/demoing in my FPGA designs.

## Current cores

### `display_serial_32bit_hex`

This core is handy for outputting/debugging 32-bit values from a design.
32 bits of data go in, and a SPI-like serial protocol for driving 8-nibble hex
values into an 8-digit, 7-segment display comes out.

## To-do list

1. Writing makefiles for each test suite and/or core is lame. I should move from raw cocotb to [cocotb-test](https://github.com/themperek/cocotb-test).
2. `display_serial_32bit_hex` needs better documentation of the display it targets.
3. `display_serial_32bit_hex` would read better with less magic numbers.
