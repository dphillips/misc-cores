#!/bin/bash -xe

for i in hdl/*.v ; do
	iverilog -tnull "$i"
	verilator --lint-only "$i"
done
